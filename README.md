#Giant Mod Comunicator API
[![Build Status](http://ci.seagiants.net:8080/job/GiantModComunicatorAPI/badge/icon)](http://ci.seagiants.net:8080/job/GiantModComunicatorAPI/)


### Maven ###

```
#!xml

<repository>
    <id>seagiants-public<id>
    <url>http://seagiants.net:8081/repository/public/</url>
</repository>

<dependency>
    <groupId>de.quantumnanox</groupId>
    <artifactId>GiantModComunicatorAPI</artifactId>
    <version>LATEST</version>
    <scope>provided</scope>
</dependency>
```

### Download binaries ###

Download from Jenkins: http://ci.seagiants.net:8080/job/GiantModComunicatorAPI


### Tutorials ###

https://www.youtube.com/playlist?list=PL5I1THcMoTW-lFJ9tVAgIauauA5CTA7sA