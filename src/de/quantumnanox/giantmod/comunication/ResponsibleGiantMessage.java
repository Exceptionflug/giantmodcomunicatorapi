
package de.quantumnanox.giantmod.comunication;

import java.io.DataInput;

/**
 * A {@link ResponsibleGiantMessage} is an {@link GiantMessage} which has two directions. A request and a reponse. Both are extending from {@link ResponsibleGiantMessage}.
 * 
 * @author Nico Britze
 *
 */
public abstract class ResponsibleGiantMessage extends GiantMessage {

    public ResponsibleGiantMessage(String subchannel) {
	super(subchannel);
    }
    
    public ResponsibleGiantMessage(String subchannel, DataInput input) {
	super(subchannel, input);
    }

    /**
     * 
     * @return The {@link Class} of the request/response {@link ResponsibleGiantMessage}
     */
    public abstract Class<? extends ResponsibleGiantMessage> getOpposite();
    
}
