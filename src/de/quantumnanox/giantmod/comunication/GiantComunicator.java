package de.quantumnanox.giantmod.comunication;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import de.quantumnanox.giantmod.comunication.messages.play.GiantPlayHUDColorResponse;
import de.quantumnanox.giantmod.comunication.messages.status.GiantStatusRequest;
import de.quantumnanox.giantmod.comunication.messages.status.GiantStatusResponse;

/**
 * This class is listening to answers on the GiantMod plugin messaging channel.
 * You can send custom messages from here to GiantMod and get responses from it.
 * 
 * @author Nico Britze
 *
 */
public class GiantComunicator implements PluginMessageListener, Listener {

    private Multimap<UUID, MessageCallback<ResponsibleGiantMessage>> callbacks = ArrayListMultimap.create();
    private HashMap<UUID, GiantPhase> gamodders = new HashMap<>();
    private HashMap<Class<? extends ResponsibleGiantMessage>, Class<? extends ResponsibleGiantMessage>> registeredPairs = new HashMap<>();

    @Override
    public void onPluginMessageReceived(String channel, Player p, byte[] data) {
	DataInput input = new DataInputStream(new ByteArrayInputStream(data));
	try {
	    String subchannel = input.readUTF();
	    if (subchannel.equals("GiantStatusResponse")) {
		GiantStatusResponse r = new GiantStatusResponse(input);
		if (isRequested(p.getUniqueId(), r)) {
		    MessageCallback<ResponsibleGiantMessage> callback = getCallback(p.getUniqueId(), r);
		    callback.accept(r);
		    callbacks.remove(p.getUniqueId(), callback);
		}
		return;
	    }
	    if (subchannel.equals("GiantPlayHUDColorResponse")) {
		GiantPlayHUDColorResponse r = new GiantPlayHUDColorResponse(input);
		if (isRequested(p.getUniqueId(), r)) {
		    MessageCallback<ResponsibleGiantMessage> callback = getCallback(p.getUniqueId(), r);
		    callback.accept(r);
		    callbacks.remove(p.getUniqueId(), callback);
		}
		return;
	    }
	    try {
		ResponsibleGiantMessage msg = getResponse(subchannel, input);
		if(msg == null) {
		    Bukkit.getPluginManager().callEvent(new GiantModCustomMessageReceivedEvent(p, subchannel, input));
		} else {
		    MessageCallback<ResponsibleGiantMessage> callback = getCallback(p.getUniqueId(), msg);
		    callback.accept(msg);
		    callbacks.remove(p.getUniqueId(), callback);
		}
	    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
		    | InvocationTargetException | NoSuchMethodException | SecurityException e) {
		e.printStackTrace();
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
    
    /**
     * Registering two messages. One request and one response. After registration you can receive responses by sending the desired request.
     * 
     * @param request - The request {@link Object}
     * @param response - The response {@link Object}
     */
    public <T extends ResponsibleGiantMessage> void registerMessagePair(T request, T response) {
	if(request.getOpposite().getName().equals(response.getClass().getName()) && response.getOpposite().getName().equals(request.getClass().getName())) {
	    try {
		response.getClass().getConstructor(DataInput.class);
		registeredPairs.put(request.getClass(), response.getClass());
	    } catch(NoSuchMethodException e) {
		throw new IllegalArgumentException("Illegal response class. Response classes have to require DataInput in its constructor and nothing else!", e);
	    }
	} else {
	    throw new IllegalArgumentException("The given messages are not linked together!");
	}
    }
    
    private ResponsibleGiantMessage getResponse(String subchannel, DataInput input) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
	for(Class<? extends ResponsibleGiantMessage> clazz : registeredPairs.keySet()) {
	    if(registeredPairs.get(clazz).getSimpleName().equals(subchannel)) {
		ResponsibleGiantMessage msg = registeredPairs.get(clazz).getConstructor(DataInput.class).newInstance(input);
		return msg;
	    }
	}
	return null;
    }

    /**
     * Sends a given message to a given player.
     * 
     * @param target - The player which is using GiantMod
     * @param message - The desired message to be sent
     * @return {@link MessageCallback} if the message is a type of {@link ResponsibleGiantMessage} or null if it is <b>not</b>.
     */
    public <T extends ResponsibleGiantMessage> MessageCallback<T> sendMessage(Player target, GiantMessage message) {

	// In this bukkit version, a strange bug is going on. We have to manually add the channel to the player.
	try {
	    Object obcPlayer = ReflectionUtil.getOBCPlayer(target);
	    Method addChannel = obcPlayer.getClass().getMethod("addChannel", String.class);
	    addChannel.invoke(obcPlayer, "GiantMod");
	} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
		| InvocationTargetException | ClassNotFoundException e) {
	    e.printStackTrace();
	}

	target.sendPluginMessage(ComunicationAPI.getInstance(), "GiantMod", message.export());
	if (message instanceof ResponsibleGiantMessage) {
	    ResponsibleGiantMessage msg = (ResponsibleGiantMessage) message;
	    MessageCallback<T> out = new MessageCallback<>(msg.getOpposite());
	    callbacks.put(target.getUniqueId(), (MessageCallback<ResponsibleGiantMessage>) out);
	    return out;
	} else {
	    return null;
	}
    }

    private boolean isRequested(UUID id, ResponsibleGiantMessage msg) {
	return getCallback(id, msg) != null;
    }

    private MessageCallback<ResponsibleGiantMessage> getCallback(UUID id, ResponsibleGiantMessage msg) {
	for (UUID idd : callbacks.keySet()) {
	    if (idd.equals(id)) {
		for (MessageCallback msgcbk : callbacks.get(id)) {
		    if (msgcbk.getTargetClass().getName().equals(msg.getClass().getName()))
			return msgcbk;
		}
	    }
	}
	return null;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
	Bukkit.getScheduler().runTaskAsynchronously(ComunicationAPI.getInstance(), new Runnable() {
	    public void run() {
		MessageCallback<GiantStatusResponse> callback = sendMessage(e.getPlayer(), new GiantStatusRequest());
		GiantStatusResponse r = callback.getValueUninterruptibly();
		if (r != null) {
		    gamodders.put(e.getPlayer().getUniqueId(), r.getPhase());
		    System.out.println(e.getPlayer().getName()+" is using GiantMod v"+r.getVersion()+" @ Phase "+r.getPhase().name());
		    if(r.getPhase() == GiantPhase.PLAY) {
			Bukkit.getPluginManager().callEvent(new GiantModUserJoinEvent(e.getPlayer(), r.getVersion()));
		    }
		}
	    }
	});
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
	if (gamodders.containsKey(e.getPlayer().getUniqueId())) {
	    gamodders.remove(e.getPlayer().getUniqueId());
	}
    }

    /**
     * Checks if a player is using GiantMod.<br>
     * <br>
     * <b>NOTICE:</b> This methos isn't checking if the player is in the Play {@link GiantPhase}.
     * 
     * @param p - The player
     * @return true if the player is using GiantMod
     */
    public boolean isUsingGiantMod(Player p) {
	return gamodders.containsKey(p.getUniqueId());
    }

    /**
     * Get the {@link GiantPhase} of a player.
     * 
     * @param p - The player
     * @return {@link GiantPhase} or null if the player isn't using GiantMod
     */
    public GiantPhase getPhase(Player p) {
	return gamodders.get(p.getUniqueId());
    }

    /**
     * A descriptor which is telling us if the player is connected with success to GiantCord via GiantMod.<br>
     * <br>
     * <b>HANDSHAKE</b> - The player isn't able to use play messages.<br>
     * <b>PLAY</b> - The player is connected with success. We can use features from GiantMod now.
     * 
     * @author Nico Britze
     *
     */
    public static enum GiantPhase {

	HANDSHAKE, PLAY;

	public int getID() {
	    if (this == HANDSHAKE) {
		return 0;
	    }
	    if (this == PLAY) {
		return 1;
	    }
	    return -1;
	}

	public static GiantPhase getPhase(int id) {
	    if (id == 0) {
		return HANDSHAKE;
	    }
	    if (id == 1) {
		return PLAY;
	    }
	    return null;
	}

    }

}
