package de.quantumnanox.giantmod.comunication;

import java.io.DataInput;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GiantModCustomMessageReceivedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final Player player;
    private DataInput input;
    private String subchannel;
    
    public GiantModCustomMessageReceivedEvent(Player p, String sub, DataInput input) {
	this.input = input;
	player = p;
	subchannel = sub;
    }

    @Override
    public HandlerList getHandlers() {
	return handlers;
    }

    public static HandlerList getHandlerList() {
	return handlers;
    }

    public DataInput getInput() {
	return input;
    }
    
    public Player getPlayer() {
	return player;
    }
    
    public String getSubchannel() {
	return subchannel;
    }

}
