package de.quantumnanox.giantmod.comunication;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * An GiantMessage is data in a {@link PacketPlayInCustomPayload} or {@link PacketPlayOutCustomPayload} to comunicate with the GiantMod client.<br>
 * As you can see, an GiantMessage can be sent or received.
 * 
 * @author Nico Britze
 *
 */
public abstract class GiantMessage {

    private String subchannel;
    private MessageDirection direction;
    private ByteArrayOutputStream bos = new ByteArrayOutputStream();
    
    protected DataOutput output = new DataOutputStream(bos);
    protected DataInput input;
    
    /**
     * Creates an instance of {@link GiantMessage} which can be <b>sent</b> to a client.
     * 
     * @param subchannel - The PluginMessage subchannel (the message name itself)
     */
    public GiantMessage(String subchannel) {
	direction = MessageDirection.OUTGOING;
	this.subchannel = subchannel;
	try {
	    output.writeUTF(subchannel);
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
    
    /**
     * Creates an instance of {@link GiantMessage} which can be <b>received</b> from clients.
     * 
     * @param subchannel - The PluginMessage subchannel (the message name itself)
     * @param input - The stream of data
     */
    public GiantMessage(String subchannel, DataInput input) {
	direction = MessageDirection.INCOMING;
	this.subchannel = subchannel;
    }
    
    public MessageDirection getDirection() {
	return direction;
    }
    
    public String getSubchannel() {
	return subchannel;
    }
    
    public byte[] export() {
	if(direction == MessageDirection.OUTGOING) {
	    return bos.toByteArray();
	} else {
	    throw new IllegalStateException("Cannot export an incoming message!");
	}
    }
    
    /**
     * A descriptor in which direction a {@link GiantMessage} is.<br><br>
     * 
     * <b>INCOMING</b> - A message is being received by the {@link GiantComunicator}<br>
     * <b>OUTGOING</b> - A message is being sent by the {@link GiantComunicator}
     * 
     * @author Nico Britze
     *
     */
    public static enum MessageDirection {
	
	INCOMING, OUTGOING;
	
    }
    
}
