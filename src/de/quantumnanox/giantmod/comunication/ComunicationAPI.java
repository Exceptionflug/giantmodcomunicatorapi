package de.quantumnanox.giantmod.comunication;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Bukkit plugin mainclass
 * 
 * @author Nico Britze
 *
 */
public class ComunicationAPI extends JavaPlugin {
    
    private GiantComunicator comunicator;

    @Override
    public void onEnable() {
	System.out.println("This is the GAModComunication api by Zero_StepZ (C) 2017");
	comunicator = new GiantComunicator();
	Bukkit.getMessenger().registerOutgoingPluginChannel(this, "GiantMod");
	Bukkit.getMessenger().registerIncomingPluginChannel(this, "GiantMod", comunicator);
	Bukkit.getPluginManager().registerEvents(comunicator, this);
    }
    
    public GiantComunicator getComunicator() {
	return comunicator;
    }
    
    public static ComunicationAPI getInstance() {
	return getPlugin(ComunicationAPI.class);
    }
    
}
