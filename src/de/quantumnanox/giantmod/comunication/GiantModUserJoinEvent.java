package de.quantumnanox.giantmod.comunication;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * An Event which is fired when the client finished the handshake with the server
 * 
 * @author Nico Britze
 *
 */
public class GiantModUserJoinEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final String giantModVersion;
    private final Player player;
    
    public GiantModUserJoinEvent(Player p, String version) {
	giantModVersion = version;
	player = p;
    }

    @Override
    public HandlerList getHandlers() {
	return handlers;
    }

    public static HandlerList getHandlerList() {
	return handlers;
    }

    public String getGiantModVersion() {
	return giantModVersion;
    }
    
    public Player getPlayer() {
	return player;
    }

}
