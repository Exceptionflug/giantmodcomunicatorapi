package de.quantumnanox.giantmod.comunication;

import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import com.avaje.ebean.validation.Future;

/**
 * A {@link MessageCallback} is a mix of a {@link Consumer} and a {@link Future}.<br>
 * It is designed to wait for an response of a sent request.
 * 
 * @author Nico Britze
 *
 * @param <T> The type of the {@link ResponsibleGiantMessage}
 */
public class MessageCallback<T extends ResponsibleGiantMessage> implements Consumer<T> {
    
    private T value;
    private boolean timedOut;
    private Class clazz;
    
    public MessageCallback(Class<? extends ResponsibleGiantMessage> clazz) {
	this(clazz, 20*5);
    }
    
    public MessageCallback(Class<? extends ResponsibleGiantMessage> clazz, long timeout) {
	this.clazz = clazz;
	Bukkit.getScheduler().runTaskLater(ComunicationAPI.getInstance(), new Runnable() {
	    public void run() {
		if(value == null) timedOut = true;
	    }
	}, timeout);
    }

    @Override
    public void accept(T t) {
	if(this.timedOut == false) this.value = t;
    }
    
    /**
     * Returns the response when it have been received. If the response isn't received yet, the current {@link Thread} will wait till its there or a timeout will appear if the response isn't received in time.
     * 
     * @return {@link ResponsibleGiantMessage} if the response has been received or null if not.
     * @throws InterruptedException If the {@link Thread} is being interrupted during waiting.
     */
    public T getValue() throws InterruptedException {
	while(value == null && timedOut == false) {
	    Thread.sleep(1);
	}
	return value;
    }
    
    /**
     * Returns the response when it have been received. If the response isn't received yet, the current {@link Thread} will wait till its there or a timeout will appear if the response isn't received in time.
     * 
     * @return {@link ResponsibleGiantMessage} if the response has been received or null if not.
     */
    public T getValueUninterruptibly() {
	while(value == null && timedOut == false) {
	    Thread.yield();
	}
	return value;
    }
    
    /**
     * The current {@link Thread} will wait till the response is received or a timeout will appear if it isn't received in time.
     * @throws InterruptedException If the {@link Thread} is being interrupted during waiting.
     */
    public void await() throws InterruptedException {
	while(value == null && timedOut == false) {
	    Thread.sleep(1);
	}
    }
    
    /**
     * The current {@link Thread} will wait till the response is received or a timeout will appear if it isn't received in time.
     * 
     */
    public void awaitUninterruptibly() {
	while(value == null && timedOut == false) {
	    Thread.yield();
	}
    }
    
    public Class getTargetClass() {
	return clazz;
    }
    
}
