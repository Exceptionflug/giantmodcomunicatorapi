package de.quantumnanox.giantmod.comunication.messages.play;

import java.io.DataInput;
import java.io.IOException;

import de.quantumnanox.giantmod.comunication.ResponsibleGiantMessage;

/**
 * The response of {@link GiantPlayHUDColorRequest}
 * 
 * @author Nico Britze
 *
 */
public class GiantPlayHUDColorResponse extends ResponsibleGiantMessage {
    
    private int color;
    private boolean custom;

    public GiantPlayHUDColorResponse(DataInput input) throws IOException {
	super("GiantPlayHUDColorResponse", input);
	color = input.readInt();
	custom = input.readBoolean();
    }

    @Override
    public Class<? extends ResponsibleGiantMessage> getOpposite() {
	return GiantPlayHUDColorRequest.class;
    }
    
    public int getColor() {
	return color;
    }
    
    public boolean isCustom() {
	return custom;
    }

}
