package de.quantumnanox.giantmod.comunication.messages.play;

import java.io.IOException;
import java.util.List;

import de.quantumnanox.giantmod.comunication.GiantMessage;
import de.quantumnanox.giantmod.cutscene.CutscenePosition;

/**
 * 
 * Plays a bauercam cutscene on the client
 * 
 * @author Nico Britze
 *
 */
public class GiantPlayCutscene extends GiantMessage {

    public GiantPlayCutscene(int frames, List<CutscenePosition> positions) {
	super("GiantPlayCutscene");
	try {
	    output.writeInt(frames);
	    output.writeInt(positions.size());
	    for(CutscenePosition pos : positions) {
		output.writeDouble(pos.x);
		output.writeDouble(pos.y);
		output.writeDouble(pos.z);
		output.writeFloat(pos.pitch);
		output.writeFloat(pos.yaw);
		output.writeFloat(pos.roll);
		output.writeFloat(pos.fov);
	    }
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

}
