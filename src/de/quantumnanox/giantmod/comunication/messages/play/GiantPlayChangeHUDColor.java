package de.quantumnanox.giantmod.comunication.messages.play;

import java.io.IOException;

import de.quantumnanox.giantmod.comunication.GiantMessage;

/**
 * Change the outline overlay color of an GiantMod user.
 * 
 * @author Nico Britze
 *
 */
public class GiantPlayChangeHUDColor extends GiantMessage {

    /**
     * Change the outline overlay color of an GiantMod user.
     *
     */
    public GiantPlayChangeHUDColor(int color, boolean fade) throws IOException {
	super("GiantPlayChangeHUDColor");
	output.writeInt(color);
	output.writeBoolean(fade);
    }

}
