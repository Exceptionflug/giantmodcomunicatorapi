package de.quantumnanox.giantmod.comunication.messages.play;

import java.io.IOException;

import de.quantumnanox.giantmod.comunication.GiantMessage;

public class GiantPlayUpdateProfileResponse extends GiantMessage {
    public GiantPlayUpdateProfileResponse(String isocodeLANGUAGE, String servername, String gamemode, String uuid)
	    throws IOException {
	super("GiantPlayUpdateProfileResponse");
	this.output.writeUTF(isocodeLANGUAGE.toLowerCase().concat(":").concat(servername.toUpperCase()));
	this.output.writeUTF(gamemode);
	this.output.writeUTF(uuid);
    }
}
