package de.quantumnanox.giantmod.comunication.messages.play;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import de.quantumnanox.giantmod.comunication.GiantMessage;

/**
 * 
 * Use this message to send a specific extension.ctf to the client.
 * 
 * @author Nico Britze
 *
 */
public class GiantPlayTransmitExtension extends GiantMessage {

    public GiantPlayTransmitExtension(File file) {
	super("GiantPlayTransmitExtension");
	try {
	    FileInputStream fis = new FileInputStream(file);
	    byte[] data = new byte[fis.available()];
	    fis.read(data);
	    fis.close();
	    output.writeInt(data.length);
	    for (int i = 0; i < data.length; i++) {
		output.writeByte(data[i]);
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

}
