package de.quantumnanox.giantmod.comunication.messages.play;

import java.io.IOException;

import de.quantumnanox.giantmod.comunication.GiantMessage;

/**
 * 
 * Used to tell giantmod that player switched server
 * 
 * @author Nico Britze
 *
 */
public class GiantPlaySwitchGamemode extends GiantMessage {
    public GiantPlaySwitchGamemode(String gamemode) throws IOException {
	super("GiantPlaySwitchGamemode");
	output.writeUTF(gamemode);
    }

}