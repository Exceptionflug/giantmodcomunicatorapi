package de.quantumnanox.giantmod.comunication.messages.play;

import de.quantumnanox.giantmod.comunication.ResponsibleGiantMessage;

/**
 * Request what outline color the client has actually
 * 
 * @author Nico Britze
 *
 */
public class GiantPlayHUDColorRequest extends ResponsibleGiantMessage {

    public GiantPlayHUDColorRequest() {
	super("GiantPlayHUDColorRequest");
    }

    @Override
    public Class<? extends ResponsibleGiantMessage> getOpposite() {
	return GiantPlayHUDColorResponse.class;
    }

}
