package de.quantumnanox.giantmod.comunication.messages.play;

import java.io.DataInput;

import de.quantumnanox.giantmod.comunication.GiantMessage;

public class GiantPlayUpdateProfileRequest extends GiantMessage	 {
    public GiantPlayUpdateProfileRequest(DataInput input) {
	super("GiantPlayUpdateProfileRequest", input);
    }
}
