package de.quantumnanox.giantmod.comunication.messages.status;

import de.quantumnanox.giantmod.comunication.ResponsibleGiantMessage;

/**
 * 
 * Request the actual status of the client
 * 
 * @author Nico Britze
 *
 */
public class GiantStatusRequest extends ResponsibleGiantMessage {

    public GiantStatusRequest() {
	super("GiantStatusRequest");
    }

    @Override
    public Class<? extends ResponsibleGiantMessage> getOpposite() {
	return GiantStatusResponse.class;
    }

}
