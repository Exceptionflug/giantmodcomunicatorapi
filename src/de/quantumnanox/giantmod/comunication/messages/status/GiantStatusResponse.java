package de.quantumnanox.giantmod.comunication.messages.status;

import java.io.DataInput;
import java.io.IOException;

import de.quantumnanox.giantmod.comunication.ResponsibleGiantMessage;
import de.quantumnanox.giantmod.comunication.GiantComunicator.GiantPhase;

/**
 * Response of {@link GiantStatusRequest}
 * 
 * @author Nico Britze
 *
 */
public class GiantStatusResponse extends ResponsibleGiantMessage {

    private GiantPhase phase;
    private String version;
    
    public GiantStatusResponse(DataInput input) throws IOException {
	super("GiantStatusResponse", input);
	phase = GiantPhase.getPhase(input.readInt());
	version = input.readUTF();
    }

    @Override
    public Class<? extends ResponsibleGiantMessage> getOpposite() {
	return GiantStatusRequest.class;
    }
    
    public GiantPhase getPhase() {
	return phase;
    }
    
    public String getVersion() {
	return version;
    }

}
