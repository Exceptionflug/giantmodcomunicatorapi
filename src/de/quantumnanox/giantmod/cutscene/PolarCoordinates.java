package de.quantumnanox.giantmod.cutscene;

/**
 * 
 * Contains two float values (yaw and pitch)
 * 
 * @author Nico Britze
 *
 */
public final class PolarCoordinates {

    public final float pitch;
    public final float yaw;

    public PolarCoordinates(final float pitch, final float yaw) {
	this.pitch = pitch;
	this.yaw = yaw;
    }

}
