package de.quantumnanox.giantmod.cutscene;

/**
 * Contains absolute world coordinates AND {@link PolarCoordinates}
 * 
 * @author Nico Britze
 *
 */
public final class CutscenePosition extends Vector3D {

    public final float pitch;
    public final float yaw;
    public final float roll;
    public final float fov;

    public CutscenePosition(final double x, final double y, final double z, final float pitch, final float yaw,
	    final float roll, final float fov) {
	super(x, y, z);
	this.pitch = pitch;
	this.yaw = yaw;
	this.roll = roll;
	this.fov = fov;
    }

    @Override
    public String toString() {
	return super.toString() + padding + this.pitch + padding + this.yaw + padding + this.roll + padding + this.fov;
    }

    public static CutscenePosition fromString(final String input) {
	final String error = "Position kann nicht verarbeitet werden " + input;

	final String[] parts = input.split(padding);
	if (parts.length != 7) {
	    return null;
	}

	try {
	    final double x = Double.parseDouble(parts[0]);
	    final double y = Double.parseDouble(parts[1]);
	    final double z = Double.parseDouble(parts[2]);
	    final float pitch = Float.parseFloat(parts[3]);
	    final float yaw = Float.parseFloat(parts[4]);
	    final float roll = Float.parseFloat(parts[5]);
	    final float fov = Float.parseFloat(parts[6]);

	    return new CutscenePosition(x, y, z, pitch, yaw, roll, fov);
	} catch (final NumberFormatException e) {
	    return null;
	}
    }

}
